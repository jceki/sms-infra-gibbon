#!/bin/bash

VOLUME_GIBBON_FOLDER="/var/sms-gibbon"
VOLUME_GIBBON_DB_FOLDER="/var/sms-gibbon-db"

STEP=1

increment_step() {
    (( STEP++ ))
}

make_dir() {
    if [ ! -d "$1" ] ; then
        mkdir -p "$1"
    else
        echo "$1 already exists, ignoring..."
    fi
}

assert_env_var() {
    [ -n "$1" ] || {
        echo "Environment variable $2 is not set"
        exit 1
    }
}

echo "$STEP) Creating volumes"
make_dir $VOLUME_GIBBON_FOLDER
make_dir $VOLUME_GIBBON_DB_FOLDER

increment_step

echo "$STEP) Checking MySQL configuration"
assert_env_var "$MYSQL_ROOT_PASSWORD" "MYSQL_ROOT_PASSWORD"
assert_env_var "$MYSQL_USER" "MYSQL_USER"
assert_env_var "$MYSQL_PASSWORD" "MYSQL_PASSWORD"
assert_env_var "$MYSQL_DATABASE" "MYSQL_DATABASE"

increment_step

echo "$STEP) Fetching intallation files"
BUILD_CONTEXT_FOLDER="/var/sms-gibbon-build"
GIT_INSTALLATION_REPO="https://bitbucket.org/jceki/sms-infra-gibbon.git"
mkdir -p $BUILD_CONTEXT_FOLDER
cd $BUILD_CONTEXT_FOLDER
find . -mindepth 1 | xargs rm -rf
git clone $GIT_INSTALLATION_REPO .

increment_step

echo "$STEP) Starting containers"
docker-compose rm -f -s
docker-compose up -d

echo "Installation is done. See the container log installation bellow. Type CTRL+C to quit"
docker-compose logs -f sms-gibbon
