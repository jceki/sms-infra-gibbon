#!/bin/bash

SITE_FOLDER="/var/www/html"

echo "Starting Gibbon..."

if [ ! "$(ls -A $SITE_FOLDER)" ] ; then
    cd $SITE_FOLDER
    echo "Fetching Gibbon..."
    echo "$GIBBON_GIT_URL ($GIBBON_RELEASE_TAG)"
    git clone $GIBBON_GIT_URL $SITE_FOLDER
    git checkout $GIBBON_RELEASE_TAG
fi

chown -R www-data:www-data $SITE_FOLDER
docker-php-entrypoint apache2-foreground
